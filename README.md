# Hugo Casildo's test to get a frontend Vue position at latouraineinc

## Clone the project
```
git clone https://gitlab.com/hugo.casildo/latouraineinc-test
```
## Navigate to directory
```
cd latouraineinc-test
```
## Install dependencies
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
