import Vue from 'vue';
import Vuex from 'vuex';
import { sections, navBarOptions, cards } from '../data/index';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    sections: [],
    navBarOptions: [],
    cards: [],
    curtain: false,
    width: 0,
  },
  getters: {
    getSections: (state) => state.sections,
    getNavBarOptions: (state) => state.navBarOptions,
    getCards: (state) => state.cards,
    getCurtain: (state) => state.curtain,
    getWidth: (state) => state.width,
  },
  mutations: {
    setSections: (state, payload) => (state.sections = payload),
    setNavBarOptions: (state, payload) => (state.navBarOptions = payload),
    setCards: (state, payload) => (state.cards = payload),
    setCurtain: (state, payload) => (state.curtain = payload),
    toggleCurtain: (state) => (state.curtain = !state.curtain),
    setWidth: (state, payload) => (state.width = payload),
  },
  actions: {
    toggleCategoriesById({ getters }, id) {
      getters.getCards.forEach((item) => {
        if (item.id === id) {
          item.showAllCategories = !item.showAllCategories;
        }
      });
    },
    fillStore({ commit }) {
      commit('setSections', sections);
      commit('setNavBarOptions', navBarOptions);
      commit('setCards', cards);
    },
  },
});
